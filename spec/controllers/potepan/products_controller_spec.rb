require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "product#show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "200レスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    it "viewファイルが正常に表示されるか" do
      expect(response).to render_template :show
    end

    it "@productが取得できているか" do
      expect(assigns(:product)).to eq product
    end

    it "@related_productsが取得できていてかつ要素数は４つのみか" do
      expect(assigns(:related_products)).to match_array related_products[0..3]
    end
  end
end
