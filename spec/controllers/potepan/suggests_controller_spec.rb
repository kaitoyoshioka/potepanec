require 'rails_helper'
require 'webmock/rspec'

RSpec.describe Potepan::SuggestsController, type: :controller do
  describe "suggests#index" do
    context "通信が成功した場合" do
      before do
        stub_request(:get, ENV['API_URL']).with(
          query: hash_including({ :keyword => "r" })
        ).to_return(
          body: ["ruby", "ruby for women", "ruby for men"].to_json,
          status: 200,
        )
        get :index, params: { keyword: 'r' }
      end

      it "200レスポンスを返すこと" do
        expect(response).to have_http_status "200"
      end
      it "検索候補を配列で返す" do
        expect(JSON.parse(response.body)).to eq ["ruby", "ruby for women", "ruby for men"]
      end
    end

    context "通信が失敗した場合" do
      before do
        stub_request(:get, ENV['API_URL']).with(
          query: hash_including({ :keyword => "r" })
        ).to_return(
          status: 500,
        )
        get :index, params: { keyword: 'r' }
      end

      it "500レスポンスを返すこと" do
        expect(response).to have_http_status "500"
      end
      it "空の配列で返ってくる" do
        expect(JSON.parse(response.body)).to eq []
      end
    end
  end
end
