require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "category#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "200レスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    it "viewファイルが正常に表示されるか" do
      expect(response).to render_template :show
    end

    it "@taxonが取得できているか" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "@taxonomiesが取得できているか" do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it "@productsが取得できているか" do
      expect(assigns(:products)).to match_array product
    end
  end
end
