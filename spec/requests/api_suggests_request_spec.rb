require 'rails_helper'

RSpec.describe Potepan::Api::SuggestsController, type: :request do
  describe "api_suggests#index" do
    let!(:suggest1) { create(:suggest, keyword: 'test1') }
    let!(:suggest2) { create(:suggest, keyword: 'test2') }
    let!(:suggest3) { create(:suggest, keyword: 'test3') }
    let!(:suggest4) { create(:suggest, keyword: 'ruby') }
    let(:headers) { { 'Authorization' => "Bearer #{ENV['API_KEY']}" } }
    let(:params) { { keyword: 't', max_num: '2' } }

    before do
      get '/potepan/api/suggests', params: params, headers: headers
    end

    context "通信が成功した時" do
      it "200レスポンスを返す" do
        expect(response).to have_http_status "200"
      end

      it "max_numの数だけ検索候補を返す" do
        json = JSON.parse(response.body)
        expect(json.size).to eq 2
        expect(json).to eq ['test1', 'test2']
      end
    end

    context "Baerer承認がされない時" do
      let(:headers) {}

      it "401エラーになる" do
        expect(response).to have_http_status "401"
      end
    end

    context "クエリにkeywordが与えられない時" do
      let(:params) {}

      it "400エラーになる" do
        expect(response).to have_http_status "400"
      end
    end

    context "max_numが数値でない時" do
      let(:params) { { keyword: 't', max_num: 't' } }

      it "500エラーになる" do
        expect(response).to have_http_status "500"
      end
    end
  end
end
