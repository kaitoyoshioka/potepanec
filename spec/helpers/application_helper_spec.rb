require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    subject { full_title(title) }

    context "引数にnilが与えられた時" do
      let(:title) { nil }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "引数に空文字が与えられた時" do
      let(:title) { "" }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "引数に文字が与えられた時" do
      let(:title) { "test" }

      it { is_expected.to eq 'test - BIGBAG Store' }
    end
  end
end
