require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:product2) { create(:product, taxons: [taxon2]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "商品カテゴリーのサイドバーが表示されているか" do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content taxon.products.count
    expect(page).to have_content taxon2.name
    expect(page).to have_content taxon2.products.count
  end

  scenario "商品が表示されているか" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario "該当しない商品は表示されていないか" do
    expect(page).not_to have_content product2.name
  end

  scenario "サイドバーからカテゴリーページへ正常に移動するか" do
    click_on taxon2.name
    expect(current_path).to eq potepan_category_path(taxon2.id)
    expect(page).to have_content product2.name
  end

  scenario "商品詳細ページへ正常に移動するか" do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end
end
