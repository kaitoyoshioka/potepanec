require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:taxon) { create(:taxon) }
  given(:taxon2) { create(:taxon) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:related_products) { create_list(:product, 5, name: "related_product", price: "18.99", taxons: [taxon]) }
  given!(:related_product) { related_products.first }
  given!(:not_relevant_product) { create(:product, taxons: [taxon2]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細ページへ訪れた際に商品情報、関連商品が正しく表示されているか" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_selector ".productBox", count: 4
    expect(page).not_to have_content not_relevant_product.name
    within '.productsContent' do
      expect(page).not_to have_content product.name
    end
  end

  scenario "一覧ページボタンをクリックしたらcategoryページへ遷移するか" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario "関連商品をクリックしたらその商品の詳細ページへ遷移するか" do
    click_on related_product.name, match: :first
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
