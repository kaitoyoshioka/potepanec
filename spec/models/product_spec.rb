require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon1) { create(:taxon) }
  let(:taxon2) { create(:taxon) }
  let!(:product1) { create(:product, taxons: [taxon1, taxon2]) }

  describe "特定の商品と同じtaxonを持っている商品を抜き出す" do
    let(:taxon3) { create(:taxon) }
    let(:taxon4) { create(:taxon) }
    let!(:product2) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product3) { create(:product, taxons: [taxon1, taxon3]) }
    let!(:product4) { create(:product, taxons: [taxon2, taxon3]) }
    let!(:product5) { create(:product, taxons: [taxon3, taxon4]) }
    let!(:products) { create_list(:product, 2, taxons: [taxon2, taxon3]) }
    let!(:related_products) { product1.related_products.limit(4) }

    context "商品と同じtaxonを持つ" do
      it "related_productsに含まれる" do
        expect(related_products).to include product2, product3, product4
      end

      it "自分自身は含まれない" do
        expect(related_products).not_to include product1
      end

      it "taxon２つが同じでも配列内では一意性が保たれている" do
        expect(related_products == related_products.uniq).to eq(true)
      end
    end

    context "商品と同じtaxonを持たない" do
      it "related_productsに含まれない" do
        expect(related_products).not_to include product5
      end
    end

    it "関連商品は４つまで表示される" do
      expect(related_products.count).to eq 4
    end
  end
end
