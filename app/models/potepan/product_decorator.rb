Spree::Product.class_eval do
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end

  scope :includes_price_images, -> { includes(master: [:default_price, :images]) }
end
