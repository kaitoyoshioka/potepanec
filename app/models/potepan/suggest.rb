class Potepan::Suggest < ApplicationRecord
  self.table_name = "potepan_suggests"

  scope :suggests_list, ->(keyword) { where("keyword LIKE ?", "#{keyword}%") }
end
