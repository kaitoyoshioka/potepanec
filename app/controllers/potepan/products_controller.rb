class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.limit(RELATED_PRODUCT_MAXIMUM_COUNT).includes_price_images
  end
end
