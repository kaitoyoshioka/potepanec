class Potepan::Api::SuggestsController < Potepan::Api::ApiController
  def index
    if params[:keyword]
      suggest = Potepan::Suggest.suggests_list(params[:keyword]).limit(params[:max_num]).pluck(:keyword)
      render json: suggest, status: 200
    else
      render plain: "bad request", status: 400
    end
  end
end
