class Potepan::Api::ApiController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods
  before_action :authenticate
  rescue_from StandardError, :with => :render_internal_server_error

  private

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, _|
      token == ENV['API_KEY']
    end
  end

  def render_unauthorized
    render plain: "unauthorized", status: 401
  end

  def render_internal_server_error
    render plain: "internal_server_error", status: 500
  end
end
