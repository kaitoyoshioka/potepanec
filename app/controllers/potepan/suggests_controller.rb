class Potepan::SuggestsController < ApplicationController
  def index
    url = ENV['API_URL']
    client = HTTPClient.new
    query = {
      keyword: suggest_params[:keyword],
      max_num: SUGGEST_MAXIMUM_COUNT,
    }
    header = { Authorization: "Bearer #{ENV['API_KEY']}" }
    request = client.get(url, query, header)
    if request.status == 200
      render json: JSON.parse(request.body)
    else
      render json: [], status: request.status
    end
  end

  private

  def suggest_params
    params.permit(:keyword)
  end
end
