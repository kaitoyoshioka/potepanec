class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes_price_images
    @taxonomies = Spree::Taxonomy.all.includes([:root])
  end
end
